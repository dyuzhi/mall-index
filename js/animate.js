//0.简单封装动画原理函数,以便后续多次使用
//1.函数将传入两个参数obj,还有target,其中,obj代表要做动画的对象是谁,target代表要做动画的目标位置
// 2.如果要让其有缓动效果,(让obj对象移动到目标位置target的过程)
// 实现思路:
// 1.每一次移动的距离变小或者变大,此处选择变小
// 2.核心算法:(目标位置-现在位置)/10作为每一步的移动距离步长
// 3.停止条件是:盒子当前位置等于目标位置
function animate(obj, target, callback) {
  // 实际开发,应注意性能问题,那么每一次执行该函数,考虑是否重开了定时器,一般情况下,是要先清除原来的定时器在开启新的定时器
  clearInterval(obj.timer)
  // 开启定时器,并给每一个不同的调用者指定定时器的不同标号"obj.timer"
  obj.timer = setInterval(function () {
    // 设置步长值,必须写在定时器内
    var step = (target - obj.offsetLeft) / 10
    // 这个步长值有可能是小数,应该'取整'(向绝对值大的值取)
    step = step > 0 ? Math.ceil(step) : Math.floor(step)
    // 到达目标位置,停止动画,停止定时器
    if (obj.offsetLeft == target) {
      clearInterval(obj.timer)
      // 当定时器结束时,判断是否有形参传过来的回调函数
      if (callback) {
        // 调用函数
        callback()
      }
    }
    //把每一次移动的距离1改成慢慢变小的值:(目标位置-现在位置)/10
    obj.style.left = obj.offsetLeft + step + 'px'
  }, 10)
}