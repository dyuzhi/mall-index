 //要实现切换,第一步找到要操作的元素li,通过queryselect先查找ul
 var nav_list = document.querySelector('.nav_list')
 //第二步,通过ul查找li
 var lis = nav_list.querySelectorAll('li')
 // 第三步查询要改变的元素
 var items = document.querySelectorAll('.item') //这是一个伪数组集合
 //用循环绑定事件函数
 for (var i = 0; i < lis.length; i++) {
   //设置属性
   lis[i].setAttribute('index', i);
   lis[i].onclick = function () {
     // 干掉其他的并清除所带的class
     for (var i = 0; i < lis.length; i++) {
       lis[i].className = '';
     }
     // 留下自己
     this.className = 'bgc'
     // 第二步:显示指定内容,点击之后显示对应内容,点那个怎么知道?给元素设置属性index,这样知道点了谁
     //一开始设置属性:setAttribute('index',i)
     var index = this.getAttribute('index')
     console.log(index);
     //隐藏所有的,包括自己
     for (var i = 0; i < items.length; i++) {
       items[i].style.display = 'none'
     }
     //再次显示自己
     items[index].style.display = 'block'
   }
 }